<?php
/*
 * @author Luiz Fernando Lidio | The New Black Studio
 * @powered by: http://www.thenewblack.me
 * 
 *            .=     ,        =.
 *   _  _   /'/    )\,/,/(_   \ \
 *    `//-.|  (  ,\\)\//\)\/_  ) |
 *    //___\   `\\\/\\/\/\\///'  /
 * ,-"~`-._ `"--'_   `"""`  _ \`'"~-,_      Múúúúúúúúúúúúúúú!
 * \       `-.  '_`.      .'_` \ ,-"~`/     Hier gibt's nichts zu sehen!!!
 *  `.__.-'`/   (-\        /-) |-.__,'
 *    ||   |     \O)  /^\ (O/  |
 *    `\\  |         /   `\    /
 *      \\  \       /      `\ /
 *       `\\ `-.  /' .---.--.\
 *         `\\/`~(, '()      ('
 *          /(O) \\   _,.-.,_)
 *         //  \\ `\'`      /
 *        / |  ||   `""""~"`
 *      /'  |__||
 *             `o 
 * 
 * 
 */

/*
 **************************************************
 **************************************************
 ADICIONANDO FULLBAR EDITOR WYSIWYG 
 **************************************************
 **************************************************
 */

function all_tinymce( $args ) {
	$args['wordpress_adv_hidden'] = false;
	return $args;
	}
add_filter( 'tiny_mce_before_init', 'all_tinymce' );

/*
 **************************************************
 **************************************************
 FIXAR EDITOR WP 
 **************************************************
 **************************************************
 */
add_action('admin_head', 'content_textarea_height');
function content_textarea_height() {
    echo'
    <style type="text/css">
        #qtrans_textarea_content_ifr{ height:460px !important; }
    </style>
    ';
}