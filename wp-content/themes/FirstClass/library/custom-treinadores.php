<?php

/*
 * @author Luiz Fernando Lidio | The New Black Studio
 * @powered by: http://www.thenewblack.me
 * 
 *            .=     ,        =.
 *   _  _   /'/    )\,/,/(_   \ \
 *    `//-.|  (  ,\\)\//\)\/_  ) |
 *    //___\   `\\\/\\/\/\\///'  /
 * ,-"~`-._ `"--'_   `"""`  _ \`'"~-,_      Múúúúúúúúúúúúúúú!
 * \       `-.  '_`.      .'_` \ ,-"~`/     Hier gibt's nichts zu sehen!!!
 *  `.__.-'`/   (-\        /-) |-.__,'
 *    ||   |     \O)  /^\ (O/  |
 *    `\\  |         /   `\    /
 *      \\  \       /      `\ /
 *       `\\ `-.  /' .---.--.\
 *         `\\/`~(, '()      ('
 *          /(O) \\   _,.-.,_)
 *         //  \\ `\'`      /
 *        / |  ||   `""""~"`
 *      /'  |__||
 *             `o 
 * 
 * 
 */

function custom_treinadores() {

    $labels = array(
        'name'                => _x( 'Treinadores', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Treinador', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Treinadores', 'text_domain' ),
        'parent_item_colon'   => __( 'Item Pai:', 'text_domain' ),
        'all_items'           => __( 'Todos os Treinadores', 'text_domain' ),
        'view_item'           => __( 'Ver Treinadores', 'text_domain' ),
        'add_new_item'        => __( 'Adicionar Novo Treinador', 'text_domain' ),
        'add_new'             => __( 'Adicionar Novo Treinador', 'text_domain' ),
        'edit_item'           => __( 'Editar Treinador', 'text_domain' ),
        'update_item'         => __( 'Atualizar Treinador', 'text_domain' ),
        'search_items'        => __( 'Procurar Treinador', 'text_domain' ),
        'not_found'           => __( 'Não encontrado', 'text_domain' ),
        'not_found_in_trash'  => __( 'Não encontrado no lixo', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'custom_treinadores', 'text_domain' ),
        'description'         => __( 'Lista de treinadores', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array( 'title' ),
        //'taxonomies'          => array( 'category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-chart-line',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array('slug' => 'lista-de-treinadores')
    );
    register_post_type( 'custom_treinadores', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_treinadores', 0 );





