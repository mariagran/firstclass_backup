<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php if ( is_category() ) {
          single_cat_title(); echo ' | '; bloginfo( 'name' );
        } elseif ( is_tag() ) {
          echo 'Tag &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
        } elseif ( is_archive() ) {
          wp_title(''); echo ' Categoria | '; bloginfo( 'name' );
        } elseif ( is_search() ) {
          echo 'Buscando por &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
        } elseif ( is_home() || is_front_page() ) {
          bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
        }  elseif ( is_404() ) {
          echo 'Página não encontrada | '; bloginfo( 'name' );
        } elseif ( is_single() ) {
          wp_title('');
        } else {
          echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
        } ?>
    </title>
    
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-precomposed.png">
    
    <?php wp_head(); ?>
  
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/slick.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/slick-theme.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/app.css" />
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59639591-1', 'auto');
  ga('send', 'pageview');

</script>

  </head>
  <body <?php body_class(); ?>>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=570999462914254";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <?php do_action('foundationPress_after_body'); ?>
  
  <div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">
  
  <?php do_action('foundationPress_layout_start'); ?>
  
  <!-- <nav class="tab-bar hide">
    <section class="left-small">
      <a class="left-off-canvas-toggle menu-icon" ><span></span></a>
    </section>
    <section class="middle tab-bar-section">
      
      <h1 class="title"><?php bloginfo( 'name' ); ?></h1>

    </section>
  </nav> -->

  <aside class="left-off-canvas-menu">
    <?php foundationPress_mobile_off_canvas(); ?>
  </aside>

        <div class="headerBar show-for-large-up" data-magellan-destination="treinadores">
         <div class="row">
	        <div class="large-12 info-topo">
	            <div class="organiza">
	                    <span class="topo-telefone"> Telefone: (41) 3224-7354 </span>
	                    <a href="https://www.facebook.com/academiafirstclassfitness/" >
	                        <img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/facebook_topo.png " class="img-topo" alt="">
	                    </a>
	            </div>
	        </div>
	    </div>
        </div>

        <!-- <div class="top-bar-container contain-to-grid show-for-medium-up">
            <nav class="top-bar" data-topbar="">
                <ul class="title-area">
                    <li class="name">
                        <h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/logotipo-first-class.png" alt=""></a></h1>
                    </li>
                </ul>
                <section class="top-bar-section">
                    <?php //foundationPress_top_bar_l(); ?>
                    <?php foundationPress_top_bar_r(); ?>
                </section>
            </nav>
        </div> -->

        <div>
          <div class="contentHeader">
            <div style="max-width: 1314px; background:#FFF;" class="row gridShadowHeader">
            <div style="max-width: 1000px; margin:0 auto;" class="row">
              <div class="large-3 columns">
              <a href="<?php echo home_url(); ?>"><img style="margin-top:8px;" src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/logotipo-first-class.png" alt=""></a>
            </div>
            <div class="large-9 columns show-for-large-up">
              <dl class="sub-nav">
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
                <dd data-magellan-arrival="treinadores"><a href="#Treinadores">Treinadores</a></dd>
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
                <dd data-magellan-arrival="estrutura"><a href="#estrutura">Estrutura</a></dd>
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
                <dd data-magellan-arrival="agenda"><a href="#agenda">Agenda</a></dd>
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
                <dd data-magellan-arrival="planos"><a href="#planos">Planos</a></dd>
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
                <dd><a href="#" data-reveal-id="contatoModal">Contato</a></dd>
                <dd class="dividerHeader"><a href="#js">&nbsp;</a></dd>
              </dl>
            </div>
            </div>
          </div>
          </div>
        </div>

<section class="container-fluid" role="document">
  <?php do_action('foundationPress_after_header'); ?>
