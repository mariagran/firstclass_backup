<?php
/*
 * @author Willerson Leonir Pundrich | Handgran Digital Marketing
 * @powered by: http://www.handgran.com
 */
?>

</section>

<div id="contatoModal" class="reveal-modal" data-reveal>
	<h2>Fale conosco</h2>
	<p>Por favor, preencha o formulário corretamente. Entraremos em contato o mais breve possível.</p>
	<form method="post" action="email.php">
		<input type="hidden" name="contato" value="contato">
		<input type="text" required="required" name="nome_completo" placeholder="Nome Completo">
		<input type="text" required="required"  name="email" placeholder="E-mail">
		<input type="text" name="telefone" placeholder="Telefone">
		<textarea name="mensagem" required="required" placeholder="Mensagem"></textarea>
		<input type="submit" name="formulario-contato" value="Enviar">									
	</form>
	<a class="close-reveal-modal">&#215;</a>
</div>

<div class="footerBG">
		<div class="row ">
			<div class="logo-rodope">
				<?php do_action('foundationPress_before_footer'); ?>
				<img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/footer-logo.png">
			</div>
		</div>
	<footer class="row">
		<div class="large-8 medium-12 small-12 columns info-rodape">
			<div class="large-12 medium-12 small-12 columns">
				<h6>MISSÃO</h6>
				<p>Oferecer serviços com qualidade, proporcionando o bem estar, saúde e qualidade de vida, através de condicionamento físico acompanhado por profissionais qualificados.</p>
				<h6>VISÃO</h6>
				<p>Ser reconhecida como uma Academia que proporciona resultados rápidos e eficientes, contribuindo para qualidade de vida, bem como a satisfação de seus alunos.</p>
				<h6>VALORES</h6>
				<ul>
					<li>· Integridade, baseada em princípios éticos e morais</li>
					<li>· Respeito à individualidade dos colaboradores e alunos</li>
					<li>· Valorização, envolvimento e comprometimento dos colaboradores.</li>
					<li>· Transparência nas comunicações.</li>
				</ul>
			</div>
				<div class="large-6 medium-6 small-12 columns">
					<h3 id="horariodefuncionamento" class="titlePlano">HORÁRIO DE FUNCIONAMENTO</h3>
					<div class="boxPlanoContent">
						<div class="contentPlano">
							<div class="left dia-semana">
							SEGUNDA À SEXTA
							</div>
							<div class="right horario-funcionamento ">
								<p>6:00 - 22:00</p>
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="contentPlanoCinza">
								<div class="left dia-semana">
									SÁBADO
								</div>
								<div class="right horario-funcionamento">
									<p>10:00 - 14:00</p>
								</div>
								<div class="clearfix"></div>

							</div>

							<div class="contentPlano">
							<div class="left dia-semana">
							DOMINGO
							</div>
							<div class="right horario-funcionamento">
								<p>FECHADO</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
			</div>

			<div class="large-6 medium-6 small-12 columns">

			<h3 class="titlePlano contato-rodape-margin">ENTRE EM  CONTATO</h3>
			<div class="boxPlanoContent">
				<div class="contentPlano">
					<div class="contato-texto">
							<p class="contato-rua">Rua Brigadeiro Franco, 1501 <br />
							Centro | Curitiba - PR</p>
						</div>
						<!-- <div class="clearfix"></div> -->

					</div>

					<div class="contentPlanoCinza">
						<div class="contato-texto">
							<p>41 3224-7354</p>
						</div>
						<div class="clearfix"></div>

					</div>

					<div class="contentPlano">
						<div class="contato-texto">
							<p><a href="mailto:contato@academiafirstclass.com.br">contato@academiafirstclass.com.br</a></p>
							</div>
							<div class="clearfix"></div>

						</div>
					</div>

					</div>
			</div>
				<div class="text-center">
					<div class="large-4 medium-12 small-12 columns">
						<div class="espaco-facebook">
							<div class="fb-page" data-href="https://www.facebook.com/academiafirstclassfitness" data-width="295" data-height="521" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/academiafirstclassfitness"><a href="https://www.facebook.com/academiafirstclassfitness">Academia First Class Fitness</a></blockquote></div></div>
						</div>
					</div>
				</div>
					<?php do_action('foundationPress_after_footer'); ?>
				</footer>

			</div>

					<div class="ultimo-footer">
						<div class="footerCopy">
							<div class="row">
								<div class="large-12 small-12">
									<div class="large-6 medium-9 small-12 columns contentCopy">
									<p>Academia First Class © 2017 - Todos os direitos reservados</p>
									</div>
									<div class="large-6 medium-3 small-12 columns img-handgran">
										<a href="http://www.handgran.com/"> <img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/Handgran_rodape.png"> </a>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="footerBGTRAP"></div>

			<a class="exit-off-canvas"></a>

			<?php do_action('foundationPress_layout_end'); ?>
		</div>
	</div>
	<?php wp_footer(); ?>
	<?php do_action('foundationPress_before_closing_body'); ?>
	<script>
		$(function() {
			$('a[href*=#]:not([href=#])').click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
							scrollTop: target.offset().top - $(".show-for-large-up").height()
						}, 1000);
						return false;
					}
				}
			});
		});
	</script>
	<script>
		$('.bannerCircuito').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			arrows: false,
			autoplaySpeed: 4000,
			lazyLoad: 'ondemand'
		});			
	</script>
	<script>



		$('.boxTreinadoresCarousel').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 2000,
			lazyLoad: 'ondemand',
			responsive: [
			{
				breakpoint: 620,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 2
				}
			}
			]
		});



	</script>

</body>
</html>