<?php

/**

 * Template Name: Sucesso

 * Description: Sucesso

 *

 * @package tresbaquecedores

 */





get_header(); ?>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<p class="titulo">Sua mensagem foi enviada com sucesso! <i class="fas fa-check"></i></p>
<a class="linkVoltar" href="https://academiafirstclass.com.br/"><i class="fas fa-angle-left"></i> Voltar para o site</a>

<style>
	p.titulo{
		margin-top: 100px;
		font-size: 40px;
		font-weight: bold;
		text-transform: uppercase;
		color: #de854b;
		text-align: center;
		font-family: 'Roboto', sans-serif;
	}
	p.titulo i{
	    color: #8BC34A;
	    font-family: 'Roboto', sans-serif;
	}
	a.linkVoltar{
		display: block;
		margin: 0 auto;
		width: 200px;
		background: #de854b;
		color: #fff;
		text-align: center;
		text-transform: uppercase;
		text-decoration: none;
		border-radius: 5px;
		padding-top: 20px;
		padding-bottom: 20px;
		font-family: 'Roboto', sans-serif;
		margin-bottom: 100px;
	}	
</style>


<?php get_footer(); ?>