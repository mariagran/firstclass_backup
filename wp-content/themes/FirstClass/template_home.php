<?php
/**
* Template Name: Home Page
*/
?>

<?php get_header(); ?>

<div class="slider hide-for-small-only">
	<div class="row">
		<div class="bannerCircuito">
			<?php
				query_posts(array(
					'post_type' => 'custom_circuito',
					'showposts' => 10000
				) );
				?>
				<?php while (have_posts()) : the_post(); ?>
					<!-- <img src="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/banner1.jpg" alt=""> -->
					<?php
				$imageArray = get_field('imagem_do_circuito'); // Array returned by Advanced Custom Fields
				$imageAlt = $imageArray['alt']; // Grab, from the array, the 'alt'
				$imageURL = $imageArray['url']; // Grab the full size version
				$imageThumbURL = $imageArray['sizes']['thumbCircuitos']; //grab from the array, the 'sizes', and from it, the 'thumbnail'
				?>

				<img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
			<?php endwhile;?>
		</div>
	</div>
</div>


<div class="respiro">
	<div class="treinadores" id="Treinadores">
		<div class="row ">
			<div class="large-12 columns">

				<h2>Treinadores</h2>

				<div class="row">

					<div class="boxTreinadoresCarousel">

					<?php
					query_posts(array(
						'post_type' => 'custom_treinadores',
						'showposts' => 10000,
						'orderby' => 'menu_order'
						) );
						?>
						<?php while (have_posts()) : the_post(); ?>
							<div class="large-3 medium-3 small-6 columns">
								<?php
								$imageArray = get_field('foto_do_treinador'); // Array returned by Advanced Custom Fields
								$imageAlt = $imageArray['alt']; // Grab, from the array, the 'alt'
								$imageURL = $imageArray['url']; // Grab the full size version
								$imageThumbURL = $imageArray['sizes']['thumbTreinadores']; //grab from the array, the 'sizes', and from it, the 'thumbnail'
								?>
								<a href="<?php the_field('url'); ?>" target="_blank">
									<img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
								</a>
								<!-- <img src="<?php the_field('foto_do_treinador'); ?>" alt=""> -->

								<h3 class="tituloTreinador">
									<?php the_field('nome_do_treinador') ?>
								</h3>
								<p class="conteudoTreinador">
									<?php the_field('conteudo_do_treinador') ?>
								</p>
							</div>
						<?php endwhile;?>

						<?php wp_reset_query(); ?>

					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="suplementos">
		<div class="row ">
			<div class="large-12 columns">
				<h2>Loja de suplementos</h2>
				<div class="row suplementosBanners">
					<div class="large-12 medium-12 small-12 columns">
						<img src="<?php the_field('banner_1_grande'); ?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

	<div class="estrutura" id="estrutura" data-magellan-destination="estrutura">
		<div class="row">
			<div class="large-12 columns">

				<div class="estrutura-respiro">
					<h2 class="estrutura-titulo">Estrutura</h2>
				</div>

				<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
			</div>
		</div>
	</div>

<div class="respiro">
	<div class="gradeAulas" id="agenda" data-magellan-destination="agenda">
		<div class="row ">
			<div class="large-12 columns">
				<div class="">
					<h2>Grade de Aulas</h2>
					<div id="lipsum">
						<?php echo do_shortcode('[tt_timetable event="fit-dance,step,zumba,pump-local-2,abdominal,pilates,jump,circuito-funcional,jiu-jitsu"event_category="abdominal,artes-marciais,circuito-funcional,fit-dance,jump,pilates,pump-local-2,step,zumba" columns="segunda,terca,quarta,quinta,sexta" filter_style="tabs" filter_label="Todas as aulas" hide_hours_column="1" show_end_hour="1" event_layout="3" row1_color="f0f0f0" box_bg_color="ffffff" box_hover_bg_color="de854b" filter_color="de854b" disable_event_url="1" text_align="left" font_custom="arial narrow"]'); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div id="planos">
	<div class="quadro-planos">
		<div class="respiro plano-cor">
			<div id="modulo_vendas">
				<div class="respiro-planos">
					<div class="row">
						<div class="">
							<h2 class="titulo-quadro-planos">PLANOS</h2>
						</div>
					</div>

					<div class="row ">
						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-1'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-1') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-2'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-2') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-3'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-3') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-4'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-4') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-5'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-5') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-6'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-6') ?>
									<img src="<?php echo $img['url']; ?>" alt="" class="img-plano-6">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-7'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-7') ?>
									<img src="<?php echo $img['url']; ?>" alt="">
								</div>
							</div>
						</a>

						<a href="#" data-reveal-id="plano-alternativo" class="consulte plano-modal">
							<div class="box-plano" >
								<div class="titulo-plano">
									<?php echo the_field('nome-plano-8'); ?>
								</div>
								<div class="box-plano-imagem">
									<?php $img = get_field('plano-8') ?>
									<img src="<?php echo $img['url']; ?>" alt="" class="img-plano-8">
								</div>
							</div>
						</a>
					</div>

					<div class="row outros-planos">
						<div class="large-2 medium-2 columns"> &nbsp; </div>
						<div class="large-4 medium-4 columns"><h4 class="texto-outros-planos">INVESTIMENTO NA SAÚDE E NA QUALIDADE DE VIDA</h4></div>
						<div class="large-5 medium-5 columns"><a href="#" data-reveal-id="plano-alternativo" class="consulte"> <h3 class="frase-contato">CONHEÇA NOSSOS PREÇOS</h3></a></div>
						<div class="large-1 medium-1 columns"> &nbsp; </div>
					</div>

					<div class="mosculacao">
						<div class="row faixa-musuculacao">
							<h4>MUSCULAÇÃO COM HORÁRIO LIVRE E AULAS DE GINÁSTICAS INCLUSAS</h4>
						</div>
					</div>

				</div>
			</div>

			<div class="kit-ativacao">
				<div class="row">
					<div class="large-3 medium-3 small-12 columns kit-ativacao-texto ">
						<div class="frase-kit" >
							<h2>KIT ATIVAÇÃO</h2>
							<p>Oferecemos um kit  completo para você começar com tudo</p>
						</div>
					</div>

					<div class="large-2 medium-3 small-12 columns kit-ativacao-img">
						<div class="avaliacao-fisica img-responsive" > </div>
					</div>

					<div class="large-2 medium-3 small-12 columns kit-ativacao-img">
						<div class="reavaliacao-fisica img-responsive" > </div>
					</div>

					<div class="large-1 medium-2 small-12 columns kit-ativacao-img">
						<div class="treino-personalizado img-responsive" > </div>
					</div>

					<div class="large-1 medium-2 small-12 columns kit-ativacao-img">
						<div class="armario-rotativo img-responsive" > </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		<div id="plano-alternativo" class="reveal-modal" data-reveal>
			<?php $page = get_page_by_path('planos-alternativos'); ?>
			<h2><?php echo $page->post_title; ?></h2>
			<p>Preencha o formulário corretamente. Entraremos em contato o mais breve possível.</p>
			<form method="post" action="email.php">
				<input type="hidden" name="contato" value="contato">
				<input type="text" required="required" name="nome_completo" placeholder="Nome Completo">
				<input type="text" required="required"  name="email" placeholder="E-mail">
				<input type="text" name="telefone" placeholder="Telefone">
				<textarea name="mensagem" required="required" placeholder="Mensagem"></textarea>
				<input type="submit" name="formulario-contato" value="Enviar">									
			</form>
			<a class="close-reveal-modal">&#215;</a>
		</div>

	<?php get_footer(); ?>